# Spring Boot Authentication, API, and Authorization
Simple project to authenticate the user registered. User devide by Admin and Trader. Admin have authorization to all end point, while trader only read listing of stock.
Private API from: https://polygon.io/

## Endpoint
1. Sign up as Admin: http://{host}:{port}/signup/admin
2. Sign up as Trader: http://{host}:{port}/signup/trader
3. Sign in: http://{host}:{port}/signin
4. Admin page (trader list): http://{host}:{port}/admin/trader-list
5. Admin page (update trader): http://{host}:{port}/admin/trader-list
6. Admin page (delete trader): http://{host}:{port}/admin/trader-list/{id}
7. Trader page (stock list): http://{host}:{port}/trader/stock-list (Admin authorize)

## Request Format Example
1. Sign up as Admin

`
{
    "username": "maria12",
    "password": "Qwerty123",
    "firstName": "Maria",
    "lastName": "Purnomo",
    "phone": "085888405053",
    "address": "Jalan Tanah Seratus",
    "birthDate": "1997-01-12",
    "gender": "FEMALE"
}
`


2. Sign up as trader

`
{
    "username": "maksalmina",
    "password": "Qwerty123",
    "firstName": "Maksal",
    "lastName": "Mina",
    "phone": "085888405053",
    "address": "Jalan Tanah Seratus",
    "birthDate": "1700-01-12",
    "gender": "MALE"
}
`

3. Sign in

`
{
    "username": "maria12",
    "password": "Qwerty123"
}
`

4. Admin page (trader list) -> need header to authenticate and authorize
Key: Authorization
Value: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJUcmFkZSBTcGhlcmUiLCJzdWIiOiI3MWJjYTE5Ni0wNmM5LTQ3MWYtYmI5My0yYjk2ZWM4NWFiZGEiLCJleHAiOjE3MTE4NDE1NzEsImlhdCI6MTcxMTc4Mzk3MSwidXNlcm5hbWUiOiJtYXJpYTEyIiwicm9sZSI6IkFETUlOIn0.0BoMKGDCMClzs3qYgM1be1wDKCZOGlQ1aGCOWr1u3tU17vd4AhpTiwSyQAIqRmPn3JucH-Hp50LoRA4W0_jHmA

5. Admin page (update trader) -> need header to authenticate and authorize
Key: Authorization
Value: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJUcmFkZSBTcGhlcmUiLCJzdWIiOiI3MWJjYTE5Ni0wNmM5LTQ3MWYtYmI5My0yYjk2ZWM4NWFiZGEiLCJleHAiOjE3MTE4NDE1NzEsImlhdCI6MTcxMTc4Mzk3MSwidXNlcm5hbWUiOiJtYXJpYTEyIiwicm9sZSI6IkFETUlOIn0.0BoMKGDCMClzs3qYgM1be1wDKCZOGlQ1aGCOWr1u3tU17vd4AhpTiwSyQAIqRmPn3JucH-Hp50LoRA4W0_jHmA

`
{
    "id": "721c879a-6a3b-4010-af55-ba50a0f91ee7",
    "firstName": "New",
    "lastName": "Name",
    "phone": "085888405053",
    "address": "Jalan",
    "birthDate": "1995-12-12",
    "gender": "MALE"
}
`

6. Admin page (delete trader) -> need header to authenticate and authorize
Key: Authorization
Value: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJUcmFkZSBTcGhlcmUiLCJzdWIiOiI3MWJjYTE5Ni0wNmM5LTQ3MWYtYmI5My0yYjk2ZWM4NWFiZGEiLCJleHAiOjE3MTE4NDE1NzEsImlhdCI6MTcxMTc4Mzk3MSwidXNlcm5hbWUiOiJtYXJpYTEyIiwicm9sZSI6IkFETUlOIn0.0BoMKGDCMClzs3qYgM1be1wDKCZOGlQ1aGCOWr1u3tU17vd4AhpTiwSyQAIqRmPn3JucH-Hp50LoRA4W0_jHmA

http://{host}:{port}/admin/trader-list/5de064cc-f7c8-47f9-b914-d278625e8316

7. Trader page (stock list) -> need header to authenticate and authorize
Key: Authorization
Value: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJUcmFkZSBTcGhlcmUiLCJzdWIiOiI3MWJjYTE5Ni0wNmM5LTQ3MWYtYmI5My0yYjk2ZWM4NWFiZGEiLCJleHAiOjE3MTE4NDE1NzEsImlhdCI6MTcxMTc4Mzk3MSwidXNlcm5hbWUiOiJtYXJpYTEyIiwicm9sZSI6IkFETUlOIn0.0BoMKGDCMClzs3qYgM1be1wDKCZOGlQ1aGCOWr1u3tU17vd4AhpTiwSyQAIqRmPn3JucH-Hp50LoRA4W0_jHmA


http://{host}:{port}/trader/stock-list