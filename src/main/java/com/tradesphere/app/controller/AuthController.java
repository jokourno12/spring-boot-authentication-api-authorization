package com.tradesphere.app.controller;

import com.tradesphere.app.model.request.AuthRequest;
import com.tradesphere.app.model.response.*;
import com.tradesphere.app.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequiredArgsConstructor
public class AuthController {
    private final AuthService authService;

    @PostMapping("/signup/admin")
    public ResponseEntity<?> signupAdmin(@RequestBody AuthRequest authRequest){
        RegisterResponse registerResponse = authService.registerAdmin(authRequest);

        CommonResponse<RegisterResponse> response = CommonResponse.<RegisterResponse>builder()
                .message("Successfully register new admin")
                .statusCode(HttpStatus.CREATED.value())
                .data(registerResponse)
                .build();
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(response);
    }

    @PostMapping("/signup/trader")
    public ResponseEntity<?> signupTrader(@RequestBody AuthRequest authRequest){
        RegisterResponse registerResponse = authService.registerTrader(authRequest);

        CommonResponse<RegisterResponse> response = CommonResponse.<RegisterResponse>builder()
                .message("Successfully register new trader")
                .statusCode(HttpStatus.CREATED.value())
                .data(registerResponse)
                .build();
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(response);
    }

    @PostMapping("/signin")
    public ResponseEntity<?> signin(@RequestBody AuthRequest request){
        LoginResponse loginResponse = authService.login(request);

        CommonResponse<LoginResponse> response = CommonResponse.<LoginResponse>builder()
                .message("Success login")
                .statusCode(HttpStatus.OK.value())
                .data(loginResponse)
                .build();
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(response);
    }

    @GetMapping("/signin/preauthorize")
    public String test(){
        return "This is signin";
    }

    @GetMapping("/trader/stock-api")
    public RestDTO connectAnotherServer(){
        RestTemplate template = new RestTemplate();
        String apiKey = "r6JV9lYulN8JZeDAqgvo0x0iMqGqgm_0";
        final String BASE_URL = "https://api.polygon.io/v2/aggs/grouped/locale/us/market/stocks/2023-01-09?adjusted=true&apiKey=" + apiKey;
        RestDTO response = template.getForObject(BASE_URL, RestDTO.class);

        return response;
    }
}
