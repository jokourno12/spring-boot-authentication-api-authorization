package com.tradesphere.app.controller;

import com.tradesphere.app.model.response.*;
import com.tradesphere.app.service.AuthService;
import com.tradesphere.app.service.StockService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class TraderController {
    private final AuthService authService;
    private final StockService stockService;

    @GetMapping("/trader/stock-list")
    public ResponseEntity<?> traderPage(){
        if(authService.isTrader() || authService.isAdmin()){
            StockListResponse stockListResponse = stockService.getAllStockApi();

            CommonResponse<StockListResponse> response = CommonResponse.<StockListResponse>builder()
                    .message("Stock list got")
                    .statusCode(HttpStatus.OK.value())
                    .data(stockListResponse)
                    .build();
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response);
        }
        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .build();
    }

}
