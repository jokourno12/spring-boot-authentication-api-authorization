package com.tradesphere.app.controller;

import com.tradesphere.app.model.entity.Trader;
import com.tradesphere.app.model.request.AuthRequest;
import com.tradesphere.app.model.response.*;
import com.tradesphere.app.service.AuthService;
import com.tradesphere.app.service.TraderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class AdminController {
    private final AuthService authService;
    private final TraderService traderService;

    @GetMapping("/admin/trader-list")
    public ResponseEntity<?> adminPageTraderList(){
        if(authService.isAdmin()){
            TraderListResponse traderListResponse = traderService.getAllTrader();

            CommonResponse<TraderListResponse> response = CommonResponse.<TraderListResponse>builder()
                    .message("Trader list got")
                    .statusCode(HttpStatus.OK.value())
                    .data(traderListResponse)
                    .build();
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response);
        }
        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .build();
    }

    @PatchMapping("/admin/trader-list")
    public ResponseEntity<?> adminPageUpdateTrader(@RequestBody Trader trader){
        if(authService.isAdmin()){
            Trader traderUpdate = traderService.updateTrader(trader);

            CommonResponse<Trader> response = CommonResponse.<Trader>builder()
                    .message("Trader updated")
                    .statusCode(HttpStatus.OK.value())
                    .data(traderUpdate)
                    .build();
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response);
        }
        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .build();
    }

    @DeleteMapping("/admin/trader-list/{id}")
    public ResponseEntity<?> adminPageDeleteTrader(@PathVariable String id){
        if(authService.isAdmin()){
            if(traderService.deleteTrader(id)){
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body("Trader with id: " + id + " has been deleted");
            }else {
                return ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .body("The trader with id: " + id + " is not found");
            }
        }
        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .build();
    }
}
