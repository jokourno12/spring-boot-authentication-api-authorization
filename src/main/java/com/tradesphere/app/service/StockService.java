package com.tradesphere.app.service;

import com.tradesphere.app.model.response.StockListResponse;

public interface StockService {
    StockListResponse getAllStockApi();
}
