package com.tradesphere.app.service;

import com.tradesphere.app.model.request.AuthRequest;
import com.tradesphere.app.model.response.LoginResponse;
import com.tradesphere.app.model.response.RegisterResponse;
import jakarta.servlet.http.HttpServletRequest;

public interface AuthService {
    RegisterResponse registerAdmin(AuthRequest authRequest);
    RegisterResponse registerTrader(AuthRequest authRequest);
    LoginResponse login(AuthRequest authRequest);
    Boolean isAdmin();
    Boolean isTrader();
}
