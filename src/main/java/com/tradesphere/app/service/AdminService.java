package com.tradesphere.app.service;

import com.tradesphere.app.model.entity.Admin;

public interface AdminService {
    Admin saveAdmin(Admin admin);
}
