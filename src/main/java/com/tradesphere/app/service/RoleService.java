package com.tradesphere.app.service;

import com.tradesphere.app.constant.EnumRole;
import com.tradesphere.app.model.entity.Role;

public interface RoleService {
    Role getOrSave(EnumRole role);
}
