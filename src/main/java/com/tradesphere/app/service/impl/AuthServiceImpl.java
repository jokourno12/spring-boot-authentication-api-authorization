package com.tradesphere.app.service.impl;

import com.tradesphere.app.constant.EnumRole;
import com.tradesphere.app.model.entity.*;
import com.tradesphere.app.model.request.AuthRequest;
import com.tradesphere.app.model.response.LoginResponse;
import com.tradesphere.app.model.response.RegisterResponse;
import com.tradesphere.app.repository.UserRepository;
import com.tradesphere.app.security.JwtUtil;
import com.tradesphere.app.service.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    private final AdminService adminService;
    private final TraderService traderService;

    private final AuthenticationManager authenticationManager;

    private final JwtUtil jwtUtil;
    private final UserService userService;
    private final HttpServletRequest request;
    @Override
    @Transactional
    public RegisterResponse registerAdmin(AuthRequest authRequest) {
        try {
            // TO DO 1: set or get role
            Role role = roleService.getOrSave(EnumRole.ADMIN);
            List<Role> roles = new ArrayList<>();
            roles.add(role);

            // TO DO 2: Set Credential or user
            User user = User.builder()
                    .username(authRequest.getUsername().toLowerCase())
                    .password(passwordEncoder.encode(authRequest.getPassword()))
                    .roles(roles)
                    .build();
            userRepository.save(user);

            // TO DO 3: Set Customer data
            Admin admin = Admin.builder()
                    .firstName(authRequest.getFirstName())
                    .lastName(authRequest.getLastName())
                    .phone(authRequest.getPhone())
                    .address(authRequest.getAddress())
                    .birthDate(authRequest.getBirthDate())
                    .gender(authRequest.getGender())
                    .user(user)
                    .build();
            adminService.saveAdmin(admin);

            // TO DO 4: Create response
            return RegisterResponse.builder()
                    .username(user.getUsername())
                    .role(role.getRole())
                    .build();
        }catch (DataIntegrityViolationException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, "User already exist");
        }
    }

    @Override
    @Transactional
    public RegisterResponse registerTrader(AuthRequest authRequest){
        try {
            Role role = roleService.getOrSave(EnumRole.TRADER);
            List<Role> roles = new ArrayList<>();
            roles.add(role);

            User user = User.builder()
                    .username(authRequest.getUsername().toLowerCase())
                    .password(passwordEncoder.encode(authRequest.getPassword()))
                    .roles(roles)
                    .build();
            userRepository.save(user);

            Trader trader = Trader.builder()
                    .firstName(authRequest.getFirstName())
                    .lastName(authRequest.getLastName())
                    .phone(authRequest.getPhone())
                    .address(authRequest.getAddress())
                    .birthDate(authRequest.getBirthDate())
                    .gender(authRequest.getGender())
                    .user(user)
                    .build();
            traderService.saveTrader(trader);

            return RegisterResponse.builder()
                    .username(user.getUsername())
                    .role(role.getRole())
                    .build();
        }catch (DataIntegrityViolationException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, "User already exist");
        }
    }

    @Override
    public LoginResponse login(AuthRequest request){
        try {
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    request.getUsername().toLowerCase(),
                    request.getPassword()
            ));

            SecurityContextHolder.getContext().setAuthentication(authentication);

            AppUser appUser = (AppUser) authentication.getPrincipal();
            String token = jwtUtil.generateToken(appUser);

            return LoginResponse.builder()
                    .token(token)
                    .role(appUser.getRole().name())
                    .build();
        }catch (AuthenticationException e){
            System.out.println("Error: " + e.getMessage());
            return LoginResponse.builder()
                    .token("Ini bermasalah")
                    .role("admin bermasalah")
                    .build();
        }
    }

    @Override
    public Boolean isAdmin() {
        String headerAuth = request.getHeader(HttpHeaders.AUTHORIZATION);

        String token = null;
        if(headerAuth != null && headerAuth.startsWith("Bearer ")){
            token = headerAuth.substring(7);
        }

        if(token != null && jwtUtil.verifyJwtToken(token)) {
            Map<String, String> userInfo = jwtUtil.getUserInfoByToken(token);

            UserDetails user = userService.loadUserByUserId(userInfo.get("userId"));
            ArrayList a = (ArrayList) user.getAuthorities();
            if(a.get(0).toString() == "ADMIN"){
                return true;
            }
        }
        return false;
    }

    @Override
    public Boolean isTrader() {
        String headerAuth = request.getHeader(HttpHeaders.AUTHORIZATION);

        String token = null;
        if(headerAuth != null && headerAuth.startsWith("Bearer ")){
            token = headerAuth.substring(7);
        }

        if(token != null && jwtUtil.verifyJwtToken(token)) {
            Map<String, String> userInfo = jwtUtil.getUserInfoByToken(token);

            UserDetails user = userService.loadUserByUserId(userInfo.get("userId"));
            ArrayList a = (ArrayList) user.getAuthorities();
            if(a.get(0).toString() == "TRADER"){
                return true;
            }
        }
        return false;
    }

}
