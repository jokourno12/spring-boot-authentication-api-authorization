package com.tradesphere.app.service.impl;

import com.tradesphere.app.model.response.RestDTO;
import com.tradesphere.app.model.response.StockListResponse;
import com.tradesphere.app.service.StockService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
@Service
@RequiredArgsConstructor
public class StockServiceImpl implements StockService {
    @Override
    public StockListResponse getAllStockApi() {
        RestTemplate template = new RestTemplate();
        String apiKey = "r6JV9lYulN8JZeDAqgvo0x0iMqGqgm_0";
        final String BASE_URL = "https://api.polygon.io/v2/aggs/grouped/locale/us/market/stocks/2023-01-09?adjusted=true&apiKey=" + apiKey;
        RestDTO restDTO = template.getForObject(BASE_URL, RestDTO.class);
        List<RestDTO> data = new ArrayList<>();
        data.add(restDTO);

        return StockListResponse.builder()
                .results(data)
                .build();
    }
}
