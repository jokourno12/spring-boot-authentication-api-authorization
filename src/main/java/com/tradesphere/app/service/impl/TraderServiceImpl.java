package com.tradesphere.app.service.impl;

import com.tradesphere.app.model.entity.Trader;
import com.tradesphere.app.model.response.TraderListResponse;
import com.tradesphere.app.repository.TraderRepository;
import com.tradesphere.app.repository.UserRepository;
import com.tradesphere.app.service.TraderService;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TraderServiceImpl implements TraderService {
    TraderRepository traderRepository;
    UserRepository userRepository;
    private JdbcTemplate jdbcTemplate;
    TraderServiceImpl(TraderRepository traderRepository, JdbcTemplate jdbcTemplate, UserRepository userRepository){
        this.traderRepository=traderRepository;
        this.userRepository=userRepository;
        this.jdbcTemplate=jdbcTemplate;
    }
    @Override
    public Trader saveTrader(Trader trader) {
        traderRepository.save(trader);
        return trader;
    }

    @Override
    public TraderListResponse getAllTrader(){
        List<Trader> res = traderRepository.findAllNativeQuery();

        return TraderListResponse.builder()
                .listOfTrader(res)
                .build();
    }

    @Override
    public Trader updateTrader(Trader trader) {
        if(traderRepository.existsById(trader.getId())){
            Trader existingTrader = traderRepository.findById(trader.getId()).orElse(null);
            existingTrader.setFirstName(trader.getFirstName());
            existingTrader.setLastName(trader.getLastName());
            existingTrader.setPhone(trader.getPhone());
            existingTrader.setAddress(trader.getAddress());
            existingTrader.setBirthDate(trader.getBirthDate());
            existingTrader.setGender(trader.getGender());
            return traderRepository.save(existingTrader);
        }else {
            throw new RuntimeException("Trader with id" + trader.getId() + "not found!");
        }
    }

    @Override
    public Boolean deleteTrader(String id) {
        if(traderRepository.findById(id).isPresent()){
            String user_id = traderRepository.findById(id).get().getUser().getId();
            traderRepository.deleteById(id);
            userRepository.deleteById(user_id);
            return true;
        }else {
            return false;
        }
    }

}
