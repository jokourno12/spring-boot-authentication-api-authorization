package com.tradesphere.app.service.impl;

import com.tradesphere.app.model.entity.Admin;
import com.tradesphere.app.repository.AdminRepository;
import com.tradesphere.app.service.AdminService;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl implements AdminService {
    AdminRepository adminRepository;
    AdminServiceImpl(AdminRepository adminRepository){
        this.adminRepository=adminRepository;
    }
    @Override
    public Admin saveAdmin(Admin admin) {
        try {
            return adminRepository.save(admin);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
