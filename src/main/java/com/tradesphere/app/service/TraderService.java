package com.tradesphere.app.service;

import com.tradesphere.app.model.entity.Trader;
import com.tradesphere.app.model.response.TraderListResponse;


public interface TraderService {
    Trader saveTrader(Trader trader);
    TraderListResponse getAllTrader();
    Trader updateTrader(Trader trader);
    Boolean deleteTrader(String id);
}
