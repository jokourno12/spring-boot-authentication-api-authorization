package com.tradesphere.app.model.entity;

import com.tradesphere.app.constant.EnumGender;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity(name = "mst_trader")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Trader {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "trader_id")
    private String id;
    private String firstName;
    private String lastName;
    private String phone;
    private String address;
    private Date birthDate;
    private EnumGender gender;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;
}
