package com.tradesphere.app.model.response;

import com.tradesphere.app.model.entity.Trader;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TraderListResponse {
    private List<Trader> listOfTrader;
}
