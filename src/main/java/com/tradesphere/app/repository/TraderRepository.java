package com.tradesphere.app.repository;

import com.tradesphere.app.model.entity.Trader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface TraderRepository extends JpaRepository<Trader, String> {
    @Query(value = "SELECT * FROM mst_trader", nativeQuery = true)
    List<Trader> findAllNativeQuery();
}
