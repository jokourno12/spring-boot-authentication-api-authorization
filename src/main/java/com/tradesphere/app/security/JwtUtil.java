package com.tradesphere.app.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.tradesphere.app.model.entity.AppUser;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtUtil {
    private String jwtSecret="rahayu";
    private String appName="Trade Sphere";
    private Long jwtExpiration=57600L;
    public String generateToken(AppUser appUser){
        try {
            Algorithm algorithm = Algorithm.HMAC512(jwtSecret.getBytes(StandardCharsets.ISO_8859_1));

            return JWT.create()
                    .withIssuer(appName)
                    .withSubject(appUser.getId())
                    .withExpiresAt(Instant.now().plusSeconds(jwtExpiration))
                    .withIssuedAt(Instant.now())
                    .withClaim("username", appUser.getUsername())
                    .withClaim("role", appUser.getRole().name())
                    .sign(algorithm);
        }catch (JWTCreationException e){
            e.printStackTrace();
            return null;
        }
    }

    public Boolean verifyJwtToken(String token){
        try {
            Algorithm algorithm = Algorithm.HMAC512(jwtSecret.getBytes(StandardCharsets.ISO_8859_1));
            JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT decodedJWT = verifier.verify(token);

            return decodedJWT.getIssuer().equals(appName);
        }catch (JWTVerificationException e){
            e.printStackTrace();
            return null;
        }
    }

    public Map<String, String> getUserInfoByToken(String token){
        try {
            Algorithm algorithm = Algorithm.HMAC512(jwtSecret.getBytes(StandardCharsets.ISO_8859_1));
            JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT decodedJWT = verifier.verify(token);

            Map<String, String> userInfo = new HashMap<>();
            userInfo.put("userId", decodedJWT.getSubject());
            userInfo.put("username", decodedJWT.getClaim("username").asString());
            userInfo.put("role", decodedJWT.getClaim("role").asString());

            return userInfo;
        }catch (JWTVerificationException e){
            throw new RuntimeException();
        }
    }
}
